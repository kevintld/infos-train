import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:infostrain/ui/infos_train.dart';
import 'package:infostrain/ui/search_train.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => const InfosTrain(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/search': (context) => SearchTrain(),
    },
    localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ],
    supportedLocales: [
      Locale('fr', ''),
      Locale('en', ''),
    ],
  ));
}