// ignore_for_file: file_names

import 'dart:convert';

import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infostrain/models/AutoCompleteBasicCity.dart';
import 'package:infostrain/models/filters.dart';
import 'package:infostrain/ui/infos_train.dart';
import 'package:intl/intl.dart';

import 'package:http/http.dart' as http;

class SideBar extends StatefulWidget {

  const SideBar( {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState()  => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  @override
  void dispose()
  {
     super.dispose();
     InfosTrain.setSelectedVilleArrivee("");
     InfosTrain.setSelectedVilleDep("");
  }
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            height: 65,
            child: const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
              ),
              child: Text(
                'Filtres de recherche',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Container(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  AutocompleteBasicCity(true), //Ville départ
                  const Padding(padding: EdgeInsets.all(8.0)),
                  AutocompleteBasicCity(false), //ville arrivée
                  const Padding(padding: EdgeInsets.all(8.0)),
                  DateTimeField(
                      decoration: const InputDecoration(
                        hintText: 'Gare de départ',
                        border: OutlineInputBorder(),
                      ),
                      selectedDate: InfosTrain.getSelectedDate(),
                      dateFormat: DateFormat('dd-MM-yyyy à kk:mm'),
                      onDateSelected: (DateTime value) {
                        setState(() {
                          InfosTrain.setSelectedDate(value);
                          InfosTrain.setSelectedTimestamp(DateFormat('yyyyMMddTkkmm').format(value).toString());
                        });
                      }),
                  const Padding(padding: EdgeInsets.all(8.0)),

                  TextButton(
                    style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Colors.white,
                        backgroundColor: Colors.blueGrey
                    ),
                    child: const Text('Rechercher'),
                    onPressed: () {
                      if(InfosTrain.getSelectedTimestamp().toString() != "" && InfosTrain.getSelectedVilleDep() != "" &&
                          InfosTrain.getSelectedVilleArrivee() != "") {
                        Navigator.pushNamed(
                          context,
                          '/search',
                          arguments: Filters(
                              InfosTrain.getSelectedTimestamp().toString(),
                              InfosTrain.getSelectedVilleDep(),
                              InfosTrain.getSelectedVilleArrivee()
                          ),
                        );
                        InfosTrain.setSelectedVilleDep("");
                        InfosTrain.setSelectedVilleArrivee("");

                      }
                    },
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }
}