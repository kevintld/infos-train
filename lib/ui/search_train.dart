import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:infostrain/ui/HeaderAppBar.dart';
import 'package:infostrain/models/filters.dart';
import 'package:infostrain/ui/infos_train.dart';
import 'package:infostrain/ui/sideBar.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:expandable/expandable.dart';

import '../models/Travel.dart';

class SearchTrain extends StatefulWidget {
  const SearchTrain({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SearchTrainState();
}

class _SearchTrainState extends State<SearchTrain> {
  DateTime? selectedDate;
  String selectedTimestamp = "";
  String selectedVilleDepart = "";
  String selectedVilleArrivee = "";
  List<Travel> listTravel = [];
  Timer? timer;
  Filters? args;

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context)!.settings.arguments == null) {
      Future(() {Navigator.pushNamed(context, "/");});
    }
    else {
      args = ModalRoute.of(context)!.settings.arguments as Filters;
    }

    return Scaffold(
          appBar : const HeaderAppBar(),
          drawer : const SideBar(),
          body: FutureBuilder<dynamic>(
            future: initTravelInformations(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              Widget children;
              if (snapshot.hasData) {
                timer?.cancel();
                listTravel = snapshot.data;
                children = _ListViewScreen();
              }
              else if (snapshot.hasError) {
                timer?.cancel();
                children = _ListErrorScreen();
              }
              else {
                timer = Timer(const Duration(seconds: 10), (){
                  Navigator.pushNamed(context, "/");
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text("Information"),
                        content: const Text("Aucun train n'a été trouvé."),
                        actions: [
                          TextButton(
                            child: const Text("OK"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    },
                  );
                });
                children = _WaitingScreen();
              }
              return children;
            }
          )
      );
  }

  Widget _ListViewScreen() {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: listTravel.length,
      itemBuilder: (BuildContext context, int index) {

        return Container(
            padding: const EdgeInsets.all(8.0),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(
                    5.0,
                    5.0,
                  ),
                  blurRadius: 10.0,
                  spreadRadius: 2.0,
                ), //BoxShadow
                BoxShadow(
                  color: Colors.white,
                  offset: Offset(0.0, 0.0),
                  blurRadius: 0.0,
                  spreadRadius: 0.0,
                ), //BoxShadow
              ],
            ),
            child: ExpandableNotifier(
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                          child: Column(
                            children: [
                              Text(
                                  listTravel[index].label,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )
                              ),
                            ],
                          )
                      ),
                    ],
                  ),
                  const Padding(padding: EdgeInsets.all(8.0)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image.asset(
                                'assets/train.jpeg',
                                height: 75,
                              )
                          ),
                        ],
                      ),
                      const Padding(padding: EdgeInsets.all(32.0)),
                      Column(
                        children: [
                          Text(
                            listTravel[index].date,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            listTravel[index].heure,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  const Padding(padding: EdgeInsets.all(8.0)),
                  if(listTravel[index].correspondance?.length != 0)
                    Expandable(
                    expanded:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: listTravel[index].correspondance?.length,
                          itemBuilder: (BuildContext context, int indexC) {
                            return Center(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      (listTravel[index].correspondance?[indexC].heure) as String,
                                      style: const TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child:
                                      Text(
                                          " - ",
                                          style:TextStyle(
                                              fontWeight: FontWeight.bold
                                          )
                                      ),
                                    ),
                                    Text(
                                      (listTravel[index].correspondance?[indexC].label) as String,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child:
                                      Text(
                                          " - ",
                                          style:TextStyle(
                                              fontWeight: FontWeight.w300
                                          )
                                      ),
                                    ),
                                    Text(
                                      (listTravel[index].correspondance?[indexC].typeRef) as String,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                        ExpandableButton(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: const [
                                Text(
                                  "Moins",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.w400
                                  ),
                                ),
                              ]
                          ),
                        )
                      ],
                    ),
                    collapsed: ExpandableButton(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: const [
                          Text(
                            "Détails",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w400
                            ),
                          ),
                        ]
                      ),
                    ),
                  )
                ],
              ),
            ),
        );
      },
      separatorBuilder: (context, index) => const SizedBox(
        height: 10,
      )
    );
  }

  Widget _ListErrorScreen()
  {
    return Center(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: EdgeInsets.only(top: 16, left: 16),
                child: Text('Aucun train trouvé.'),
              )
            ]
        )
    );
  }

  Widget _WaitingScreen()
  {
    return Center(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children : const [
              SizedBox(
                width: 60,
                height: 60,
                child: CircularProgressIndicator(),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16, left: 16),
                child: Text('Recherche de trains...'),
              )
            ]
        )
    );
  }

  Future<dynamic> initTravelInformations() async
  {
    if (args != null) {
      if (args?.villeDep != "" && args?.villeArr != "")
      {
        List<Travel> travels = [];
        Travel? global;

        String link = "https://api.sncf.com/v1/coverage/sncf/journeys?from=admin:fr:${args?.villeDep}&to=admin:fr:${this.args?.villeArr}&datetime=${args?.dateDep}";

        for(var i = 0; i<10; i++)
        {
            final response = await http.get(
                Uri.parse(link),
                headers: <String, String>{ 'Authorization': 'c976023f-1e36-4ffe-8d06-352bb91d0dad',},
            );
            if (response.statusCode == 200) {
              Map<String, dynamic> json = jsonDecode(response.body);
              for (var value in json['links']) {
                if (value["type"] == "next")
                {
                  link = value["href"];
                }
            }
              bool isDep = true;
              String labelGlobal = "";
              String arrival = "";

              for (var value in json["journeys"][0]["sections"]) {
                if (value['display_informations'] != null) {
                  if (isDep) {
                    String dateDep = json["journeys"][0]["departure_date_time"];
                    String heures_dateDep = "${dateDep.substring(9, 11)} h ${dateDep.substring(11, 13)}";
                    String jour_dateDep = dateDep.substring(6, 8);
                    String dateArr = json["journeys"][0]["arrival_date_time"];
                    String heures_dateArr = "${dateArr.substring(9, 11)} h ${dateArr.substring(11, 13)}";
                    String jour_dateArr = dateArr.substring(6, 8);

                    String heures = "${heures_dateDep} - ${heures_dateArr}";
                    String jour = jour_dateArr != jour_dateDep
                        ? "${jour_dateDep}-${jour_dateArr}/${dateArr.substring(4, 6)}/${dateArr.substring(0, 4)}"
                        : "${jour_dateDep}/${dateArr.substring(4, 6)}/${dateArr.substring(0, 4)}";
                    labelGlobal += "${value["from"]["stop_point"]["name"]} - ";

                    List<Travel> correspondances = [];
                    global = Travel(labelGlobal, jour, heures, "", correspondances);
                    isDep = false;
                  }

                    String dateDep = value["departure_date_time"];
                    String heures_dateDep = "${dateDep.substring(9, 11)} h ${dateDep.substring(11, 13)}";
                    String jour_dateDep = dateDep.substring(6, 8);
                    String dateArr = value["arrival_date_time"];
                    String heures_dateArr = "${dateArr.substring(9, 11)} h ${dateArr.substring(11, 13)}";
                    String jour_dateArr = dateArr.substring(6, 8);

                    String heures = "${heures_dateDep} - ${heures_dateArr}";
                    String jour = jour_dateArr != jour_dateDep
                        ? "${jour_dateDep}-${jour_dateArr}/${dateArr.substring(4, 6)}/${dateArr.substring(0, 4)}"
                        : "${jour_dateDep}/${dateArr.substring(4, 6)}/${dateArr.substring(0, 4)}";
                    String villes = "${value["from"]["stop_point"]["name"]} - ${value["to"]["stop_point"]["name"]}";
                    String desc = "${value["display_informations"]["commercial_mode"]} - ${value["display_informations"]["trip_short_name"]}";

                    global?.correspondance?.add(Travel.correspondance(villes, jour, heures, desc));
                    arrival = value["to"]["stop_point"]["name"];

                }
              }
              if(global?.correspondance?.length == 1) global?.correspondance?.clear();

              global?.label +=arrival;

              if(global != null ) travels.add(global);

            }
            else {
              throw Exception('Failed to load API');
            }
        }

        return travels;
      }
    }
  }
}

