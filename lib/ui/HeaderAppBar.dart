// ignore_for_file: file_names

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderAppBar extends StatelessWidget with PreferredSizeWidget{
  const HeaderAppBar({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return  AppBar(
        backgroundColor: Colors.blueGrey,
        title: GestureDetector(
          onTap: (){
            Navigator.pushNamed(context, "/");
          },
          child: Text('Infos Train'),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {Scaffold.of(context).openDrawer();},
            );
          },
        ),
        centerTitle: true,
      );
  }
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

}
