import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:infostrain/ui/HeaderAppBar.dart';
import 'package:infostrain/ui/sideBar.dart';
import 'package:carousel_slider/carousel_slider.dart';

class InfosTrain extends StatefulWidget {
  const InfosTrain({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InfosTrainState();

  static setSelectedVilleDep(String cp)
  {
    _InfosTrainState.selectedVilleDepart = cp;
  }
  static setSelectedVilleArrivee(String cp)
  {
    _InfosTrainState.selectedVilleArrivee = cp;
  }
  static setSelectedDate(DateTime selectedDate)
  {
    _InfosTrainState.selectedDate = selectedDate;
  }
  static setSelectedTimestamp(String selectedTimestamp)
  {
    _InfosTrainState.selectedTimestamp = selectedTimestamp;
  }
  static getSelectedVilleDep()
  {
    return _InfosTrainState.selectedVilleDepart;
  }
  static getSelectedVilleArrivee()
  {
    return _InfosTrainState.selectedVilleArrivee;
  }
  static getSelectedDate()
  {
    return _InfosTrainState.selectedDate ;
  }
  static getSelectedTimestamp()
  {
    return  _InfosTrainState.selectedTimestamp ;
  }
}

class _InfosTrainState extends State<InfosTrain> 
{
  static DateTime selectedDate = DateTime.now();
  static String selectedTimestamp = DateFormat('yyyyMMddTkkmm').format( DateTime.now()).toString();
  static String selectedVilleDepart = "";
  static String selectedVilleArrivee = "";

  List<String> instructions = [
    "Accédez à la section de recherche",
    "Renseignez une gare de départ",
    "Renseignez une gare d'arrivée",
    "Renseignez une date",
    "Votre liste de train est disponible !"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar : HeaderAppBar(),
        drawer : SideBar(),
        body: Container(
          child: Column(
            children: [
              CarouselSlider(
                options: CarouselOptions(
                  height: 200.0,
                  enableInfiniteScroll: true,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                ),
                items: ["banner1.jpg","banner2.jpg","banner3.jpg"].map((image) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          child: Image.asset('assets/${image}',),
                      );
                    },
                  );
                }).toList(),
              ),
              const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text(
                  "Recherche ton prochain train !",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(12.0),
                child: Column(
                  children: const [
                    Text(
                      "Instructions",
                      style: TextStyle(
                        fontSize: 20,
                        decoration: TextDecoration.underline,
                        color: Colors.blueGrey,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top:8.0),),
                  ],
                ),
              ),
              Expanded(
                child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: instructions.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: const EdgeInsets.symmetric(horizontal: 5.0),
                            padding: const EdgeInsets.all(8.0),
                            decoration: const BoxDecoration(
                              color: Colors.blueGrey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10)
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white,
                                  offset: Offset(
                                    5.0,
                                    5.0,
                                  ),
                                  blurRadius: 10.0,
                                  spreadRadius: 2.0,
                                ), //BoxShadow
                                BoxShadow(
                                  color: Colors.white,
                                  offset: Offset(0.0, 0.0),
                                  blurRadius: 0.0,
                                  spreadRadius: 0.0,
                                ), //BoxShadow
                              ],
                            ),
                            child: SizedBox(
                                height: 100,
                                child: Center(
                                  child:Text(
                                    "${index+1}. ${instructions[index]}",
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                            ),
                          );
                        },
                        separatorBuilder: (context, index) => const SizedBox(
                          height: 10,
                        )
                    ),
              )
            ],
          )
        ),
    );
  }
}