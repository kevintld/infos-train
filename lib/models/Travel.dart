// ignore_for_file: file_names

import 'package:flutter/material.dart';

@immutable
class Travel {
  late  String label;
  final String date;
  final String heure;
  final String typeRef;
  final List<Travel>? correspondance;

  Travel(label,date,heure,typeRef,correspondance):
        this.label = label,
        this.date = date,
        this.heure = heure,
        this.typeRef = typeRef,
        this.correspondance = correspondance;

  Travel.correspondance(label,date,heure,typeRef):
        this.label = label,
        this.date = date,
        this.heure = heure,
        this.typeRef = typeRef,
        this.correspondance = null;



  @override
  String toString() {
    return '$label, $date, $heure, $typeRef,$correspondance';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is Travel && other.label == label && other.date == date && other.heure == heure;
  }
}