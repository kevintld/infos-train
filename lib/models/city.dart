import 'package:flutter/material.dart';

@immutable
class City {
  final String name;
  final String cp;
  const City(name,cp):
        this.name = name,
        this.cp = cp;



  @override
  String toString() {
    return '$name, $cp';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is City && other.name == name && other.cp == cp;
  }

  @override
  int get hashCode => hashValues(cp, name);
}