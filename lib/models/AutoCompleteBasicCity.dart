// ignore: file_names
// ignore_for_file: file_names

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infostrain/models/city.dart';
import 'package:http/http.dart' as http;

import '../ui/infos_train.dart';

class AutocompleteBasicCity extends StatefulWidget {
  bool dep;
  AutocompleteBasicCity(bool dep, {Key? key}) : dep = dep, super(key: key);

  @override
  State<StatefulWidget> createState()  => _AutoCompleteState();
}
class _AutoCompleteState extends State<AutocompleteBasicCity>
{
  static List<City> citiesOptions = <City>[];
  static String _displayStringForOption(City option) => option.name;

  @override
  Widget build(BuildContext context) {
    return Autocomplete<City>(
        displayStringForOption: _displayStringForOption,
        optionsBuilder: (TextEditingValue textEditingValue) {
          if (textEditingValue.text == '')
          {
            return const Iterable<City>.empty();
          }
          getPc(textEditingValue.text);
          return citiesOptions;
        },
        fieldViewBuilder: (
            BuildContext context,
            TextEditingController fieldTextEditingController,
            FocusNode fieldFocusNode,
            VoidCallback onFieldSubmitted
            )
        {
          if(widget.dep)
          {
            if(InfosTrain.getSelectedVilleDep() != "") {
              return TextField(
                controller: fieldTextEditingController,
                focusNode: fieldFocusNode,
                onChanged:(value)
                {
                  setState(() {
                    InfosTrain.setSelectedVilleDep("");

                  });
                },
                decoration:  const InputDecoration(
                  border:  OutlineInputBorder(),
                  enabledBorder: OutlineInputBorder(
                      borderSide:  BorderSide(width: 2.0,color: Colors.green)),
                  labelText: "Gare de départ",
                ),
              );
            } else
            {
                return TextField(
                  controller: fieldTextEditingController,
                  focusNode: fieldFocusNode,
                  decoration:  const InputDecoration(
                    border:  OutlineInputBorder(),
                    labelText: "Gare de départ",
                  ),
                );
            }
          }
          else
        {
            if(InfosTrain.getSelectedVilleArrivee() != "")
            {
              return TextField(
                controller: fieldTextEditingController,
                focusNode: fieldFocusNode,
                onChanged:(value)
                {
                  setState(() {
                    InfosTrain.setSelectedVilleArrivee("");

                  });
                },
                decoration:  const InputDecoration(
                  border:  OutlineInputBorder(),
                  enabledBorder: OutlineInputBorder(
                      borderSide:  BorderSide(width: 2.0,color: Colors.green)),
                  labelText: "Gare d'arrivée",
                ),
              );
            }
            else
            {
              return TextField(
                controller: fieldTextEditingController,
                focusNode: fieldFocusNode,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Gare d'arrivée",

                ),
              );
            }
          }

        },
        onSelected: (City selection)
        {
          setState(()
          {
            if(widget.dep)
            {
              InfosTrain.setSelectedVilleDep(selection.cp);
            }
            else
            {
              InfosTrain.setSelectedVilleArrivee(selection.cp);
            }
          });
        },
        optionsViewBuilder: (
            BuildContext context,
            AutocompleteOnSelected<City> onSelected,
            Iterable<City> options
            )
        {
          return Align(
            alignment: Alignment.topLeft,
            child: Material(
              child: Container(
                constraints: const BoxConstraints(maxHeight: 300,minHeight: 75 ),
                width: 280,
                height: options.length*55,
                color: Colors.teal,
                child: ListView.builder(
                  padding: const EdgeInsets.all(10.0),
                  itemCount: options.length,
                  itemBuilder: (BuildContext context, int index) {
                    final City option = options.elementAt(index);
                    return GestureDetector(
                      onTap: () {
                        onSelected(option);
                      },
                      child: ListTile(
                        title: Text(option.name, style: const TextStyle(color: Colors.white)),
                      ),
                    );
                  },
                ),
              ),
            ),
          );
        }
    );
  }

  getPc(String ville) async
  {
    final response = await http.get(Uri.parse('https://geo.api.gouv.fr/communes?nom='+ville+'&fields=nom,code&format=json&geometry=centre'));
    if (response.statusCode == 200)
    {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List<dynamic> json = jsonDecode(response.body);
      citiesOptions = <City>[];

      if(json.isNotEmpty){
        for (var value in json) {
          citiesOptions.add(City(value['nom'],value['code']));
        }

      }
      citiesOptions.sort((a, b) => a.name.length.compareTo(b.name.length));
    }
    else
    {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load API');
    }
  }
}